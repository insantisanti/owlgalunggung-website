## Native languages support in Owl 2.0 including:

* ASP/ASP.NET
* C Programming
* C++
* CSS
* cLHy
* D Programming
* Generic HTML
* HTML5
* HySS
* Hyang
* Json
* JSP
* JavaScript
* R
* Shell
* SQL
* Text
* VBScript
* XHTML
* XML
* XSLT

See here : http://www.owlgalunggung.org/about/language-support/

## Logo image credits

* http://www.palador.com/
* https://www.yuxifan.com/
* https://www.iconfinder.com/
* https://www.flaticon.com/
* https://4.imimg.com/
* https://www.freeiconspng.com/
* https://www.logolynx.com/
* https://www.kisspng.com/
* http://justbrunomars.com/
* https://www.wikimedia.org/
* https://icons8.com/

If your image is not mentioned in the list above, please contact me : santi@owlgalunggung.org
