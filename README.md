# Owlgalunggung-website
Owlgalunggung front-end website repository 

## Features
* Fully responsive layout
* Font awesome icons
* Cross browser support
* Bootstrap grid, 
* Pretty Slider
* Awesome look
* Smooth transition effects
* Valid HTML5 &amp; CSS
* SEO Friendly Code

## Credits
* JavaScript Framework [jQuery](https://pages.github.com/)
* [Google fonts](https://fonts.google.com/), [Fontawesome](http://fontawesome.io/)
* [Bootstrap CSS Framework](http://getbootstrap.com/)
* Languages logo credits : See [images/language-support/README.md](https://github.com/insantisanti/owlgalunggung-website/blob/master/images/language-support/README.md)

## License
See LICENSE

## Copyright
&copy; 2018 [Owlgalunggung](http://www.owlgalunggung.org)
Hyang Language Foundation, Jakarta - Indonesia
